from flask import Flask, render_template, make_response
from io import StringIO
import database
import csv

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template('home.html')

@app.route('/csv')
def download():
    f = StringIO()
    writer = csv.writer(f, quotechar='"', quoting=csv.QUOTE_ALL, lineterminator="\n")
    writer.writerow([1, 2, 3, 4])
    res = make_response()
    res.data = f.getvalue()
    res.headers['Content-Type'] = 'text/csv'
    res.headers['Content-Disposition'] = 'attachment; filename=test.csv'
    return res


if __name__ == "__main__":
    database.init_db()
    app.run(debug=True)